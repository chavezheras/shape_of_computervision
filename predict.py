#!/usr/bin/env python
# coding: utf-8

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#Inference" data-toc-modified-id="Inference-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Inference</a></span></li><li><span><a href="#Imports" data-toc-modified-id="Imports-2"><span class="toc-item-num">2&nbsp;&nbsp;</span>Imports</a></span></li><li><span><a href="#Variables" data-toc-modified-id="Variables-3"><span class="toc-item-num">3&nbsp;&nbsp;</span>Variables</a></span></li><li><span><a href="#Functions" data-toc-modified-id="Functions-4"><span class="toc-item-num">4&nbsp;&nbsp;</span>Functions</a></span></li><li><span><a href="#Operations" data-toc-modified-id="Operations-5"><span class="toc-item-num">5&nbsp;&nbsp;</span>Operations</a></span></li></ul></div>

# # Inference
# This script takes in a trained model checkpoint and an image path, and outputs a focal length prediction.

# # Imports

# In[1]:


import torch
import torch.nn as nn
from torchvision.transforms import transforms
from torch.autograd import Variable
import requests
import shutil
from io import open
import os
from PIL import Image
import json
import matplotlib.pyplot as plt
import PIL
from PIL import Image, ImageOps


# # Variables

# In[2]:


target_size = (512, 340) # needs to be like the model
classes = ['tele', 'wide']
model_to_load = input("Path to model (e.g. 'models/LensClass_v0.2_model_13.model'), or press <Enter> for default model:")
if model_to_load == '':
    model_to_load = 'models/LensClass_v0.2_model_13.model'
image_path = input("Path to image (e.g. 'data/images/inference/28mm_london.jpg'):")
checkpoint = torch.load(model_to_load, map_location=torch.device('cpu')) # load checkpoint


# In[3]:


# unit class to simplify network model
class Unit(nn.Module):
    def __init__(self,in_channels,out_channels):
        super(Unit,self).__init__()
        

        self.conv = nn.Conv2d(in_channels=in_channels,kernel_size=3,out_channels=out_channels,stride=1,padding=1)
        self.bn = nn.BatchNorm2d(num_features=out_channels)
        self.relu = nn.ReLU()

    def forward(self,input):
        output = self.conv(input)
        output = self.bn(output)
        output = self.relu(output)

        return output


# In[4]:


# main network model
class SimpleNet(nn.Module):
    def __init__(self,num_classes=len(classes)):
        super(SimpleNet,self).__init__()

        #input bacth size is a tensor shaped 4, 1, 340, 512
        #Create 11 layers of the unit with max pooling in between
        self.unit1 = Unit(in_channels=1,out_channels=16)
        self.unit2 = Unit(in_channels=16, out_channels=16)
        self.unit3 = Unit(in_channels=16, out_channels=16)

        # h, w are halved by pooling to 170, 256
        self.pool1 = nn.MaxPool2d(kernel_size=2) 

        self.unit4 = Unit(in_channels=16, out_channels=32)
        self.unit5 = Unit(in_channels=32, out_channels=32)
        self.unit6 = Unit(in_channels=32, out_channels=32)
        self.unit7 = Unit(in_channels=32, out_channels=32)

        # h, w are halved by pooling to 85, 128
        self.pool2 = nn.MaxPool2d(kernel_size=2)

        self.unit8 = Unit(in_channels=32, out_channels=64)
        self.unit9 = Unit(in_channels=64, out_channels=64)
        self.unit10 = Unit(in_channels=64, out_channels=64)
        self.unit11 = Unit(in_channels=64, out_channels=64)
        # outputh here is 64 * 85 * 128

        self.avgpool = nn.AvgPool2d(kernel_size=(5,4)) #note the asymetric kernel window
        #output here is 64 * 17 * 32
        
        #Add all the units into the Sequential layer in exact order
        self.net = nn.Sequential(self.unit1, self.unit2, self.unit3,                                  self.pool1, self.unit4, self.unit5,                                  self.unit6, self.unit7, self.pool2,                                  self.unit8, self.unit9, self.unit10,                                  self.unit11, self.avgpool)

        self.fc = nn.Linear(in_features = 64*17*32, out_features = len(classes))

    def forward(self, input):
        output = self.net(input)
        output = output.view(-1, 64*17*32)
        output = self.fc(output)
        return output


# # Functions

# In[21]:


# Main prediction function
def predict_image(image_path):
    print("Preprocessing image...")
    image = Image.open(image_path)
    w, h = image.size
    #print(id)
    print("Original image size is:", w, 'x', h, 'px')

    #Rotate if in portrait mode
    if h > w:
        angle = 90
        image = image.rotate(angle, expand=True)
    
    #resize and pad
    original_size = image.size  # old_size[0] is in (width, height) format
    ratio = float(max(target_size))/max(original_size)
    new_size = tuple([int(x*ratio) for x in original_size])
    image = image.resize(new_size, Image.ANTIALIAS)
    
    # create a new image and paste the resized on it
    new_im = Image.new('RGB', (target_size[0], target_size[1]))
    new_im.paste(image, ((target_size[0] - new_size[0])//2,(target_size[1] - new_size[1])//2))
    w, h = new_im.size
    print("Resized image is:", w, 'x', h, 'px')
    
    print("Prediction in progress...")
    transformation = transforms.Compose([
        transforms.Grayscale(num_output_channels=1),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5],
                             std=[0.5])        
    ])

    # Preprocess the image
    image_tensor = transformation(new_im).float()
    # Add an extra batch dimension since pytorch treats all images as batches
    image_tensor = image_tensor.unsqueeze_(0)

    if torch.cuda.is_available():
        image_tensor.cuda()

    # Turn the input into a Variable
    input = Variable(image_tensor)

    # Predict the class of the image
    output = model(input)

    index = output.data.numpy().argmax()
    
    if index == 0:
        label = 'Telephoto lens (135mm or more)'
    elif index == 1:
        label = 'Wide angle lens (24mm or less)'
        
    print("Predicted focal lenght:", label)

    return label


# # Operations

# In[11]:


model = SimpleNet(num_classes=len(classes))
model.load_state_dict(checkpoint)
model.eval()


# In[19]:


predict_image(image_path)

