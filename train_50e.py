#!/usr/bin/env python
# coding: utf-8

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#Lens-image-Classifier-overview" data-toc-modified-id="Lens-image-Classifier-overview-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Lens image Classifier overview</a></span></li><li><span><a href="#Imports" data-toc-modified-id="Imports-2"><span class="toc-item-num">2&nbsp;&nbsp;</span>Imports</a></span></li><li><span><a href="#Variables" data-toc-modified-id="Variables-3"><span class="toc-item-num">3&nbsp;&nbsp;</span>Variables</a></span></li><li><span><a href="#Hyper-parameters" data-toc-modified-id="Hyper-parameters-4"><span class="toc-item-num">4&nbsp;&nbsp;</span>Hyper parameters</a></span></li><li><span><a href="#Classes" data-toc-modified-id="Classes-5"><span class="toc-item-num">5&nbsp;&nbsp;</span>Classes</a></span></li><li><span><a href="#Functions" data-toc-modified-id="Functions-6"><span class="toc-item-num">6&nbsp;&nbsp;</span>Functions</a></span></li><li><span><a href="#Operations" data-toc-modified-id="Operations-7"><span class="toc-item-num">7&nbsp;&nbsp;</span>Operations</a></span><ul class="toc-item"><li><span><a href="#Load-data" data-toc-modified-id="Load-data-7.1"><span class="toc-item-num">7.1&nbsp;&nbsp;</span>Load data</a></span></li><li><span><a href="#Model-setup" data-toc-modified-id="Model-setup-7.2"><span class="toc-item-num">7.2&nbsp;&nbsp;</span>Model setup</a></span></li><li><span><a href="#Train-network" data-toc-modified-id="Train-network-7.3"><span class="toc-item-num">7.3&nbsp;&nbsp;</span>Train network</a></span></li></ul></li></ul></div>

# # Lens image Classifier overview
# 
# This notebook creates two lists of [Flickr](https://www.flickr.com/ "Flickr Homepage") IDs based on their lens/focal length, and downloads the corresponding images using the URLs from the [Visual Genome project](https://visualgenome.org/api/v0/api_home.html "Download Data").
# It requires the following:
# * The uncompressed `image_data.json` file to be in the `/data` directory of the project. This file can be downloaded from [here](https://visualgenome.org/api/v0/api_home.html "Download Data").
# * The dataset `photo_dataset` found in the `/data` directory created using the `analyse.ipynb` notebook.
# 

# # Imports

# In[1]:


from comet_ml import Experiment
#import pandas as pd
from tqdm import tqdm
#import pickle
import random
#import wget
import time
import warnings
#import json
import numpy as np
import os
#import shutil
#import subprocess
#import cv2
import matplotlib.pyplot as plt
import PIL
from PIL import Image, ImageOps
import torch
import torchvision
from torchvision import transforms, datasets
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils import data
from torch.autograd import Variable
from torch.optim import Adam


# # Variables #

# In[2]:


p_name = 'FocalLength_classifier'
data_dir = 'data/ML_export/RAW_images/'
target_data_dir = 'data/ML_export/preprocessed_images/'
classes = ['tele', 'wide']
validation_split = 25 # test data split (as a percentage)
target_size = (512, 340)
batch_size = 24
epochs = 50
#%matplotlib inline
#plt.rcParams["figure.figsize"] = [20, 10]
#plt.style.use('ggplot')


# # Hyper parameters

# In[3]:


hyper_params = {
    "input_size": target_size,
    "num_classes": 2,
    "batch_size": batch_size,
    "num_epochs": epochs,
    "learning_rate": 0.001
}

experiment = Experiment('j3H5Jakl7WYKoE7CZNpg7Utgd', project_name=p_name)
experiment.log_parameters(hyper_params)


# # Classes #

# In[4]:


class Unit(nn.Module):
    def __init__(self,in_channels,out_channels):
        super(Unit,self).__init__()
        

        self.conv = nn.Conv2d(in_channels=in_channels,kernel_size=3,out_channels=out_channels,stride=1,padding=1)
        self.bn = nn.BatchNorm2d(num_features=out_channels)
        self.relu = nn.ReLU()

    def forward(self,input):
        output = self.conv(input)
        output = self.bn(output)
        output = self.relu(output)

        return output


# In[5]:


class SimpleNet(nn.Module):
    def __init__(self,num_classes=2):
        super(SimpleNet,self).__init__()

        #input bacth size is a tensor shaped 4, 1, 340, 512
        #Create 11 layers of the unit with max pooling in between
        self.unit1 = Unit(in_channels=1,out_channels=16)
        self.unit2 = Unit(in_channels=16, out_channels=16)
        self.unit3 = Unit(in_channels=16, out_channels=16)

        # h, w are halved by pooling to 170, 256
        self.pool1 = nn.MaxPool2d(kernel_size=2) 

        self.unit4 = Unit(in_channels=16, out_channels=32)
        self.unit5 = Unit(in_channels=32, out_channels=32)
        self.unit6 = Unit(in_channels=32, out_channels=32)
        self.unit7 = Unit(in_channels=32, out_channels=32)

        # h, w are halved by pooling to 85, 128
        self.pool2 = nn.MaxPool2d(kernel_size=2)

        self.unit8 = Unit(in_channels=32, out_channels=64)
        self.unit9 = Unit(in_channels=64, out_channels=64)
        self.unit10 = Unit(in_channels=64, out_channels=64)
        self.unit11 = Unit(in_channels=64, out_channels=64)
        # outputh here is 64 * 85 * 128

        self.avgpool = nn.AvgPool2d(kernel_size=(5,4)) #note the asymetric kernel window
        #output here is 64 * 17 * 32
        
        #Add all the units into the Sequential layer in exact order
        self.net = nn.Sequential(self.unit1, self.unit2, self.unit3,                                  self.pool1, self.unit4, self.unit5,                                  self.unit6, self.unit7, self.pool2,                                  self.unit8, self.unit9, self.unit10,                                  self.unit11, self.avgpool)

        self.fc = nn.Linear(in_features = 64*17*32, out_features = len(classes))

    def forward(self, input):
        output = self.net(input)
        output = output.view(-1, 64*17*32)
        output = self.fc(output)
        return output


# In[6]:


#NeTest = SimpleNet()
#print(NeTest)


# # Functions

# In[7]:


# main preporcessing function
# takes images from a folder structure /data/images/class1, class2
# loads images, rotates them, padds them, flips them and duplicates them
# outputs preprocessed images to a different folder of the same structure
def preprocess_training_data():
    for clss in classes:
        path = os.path.join(data_dir, clss) # e.g data/ML_export/RAW_images/tele
        target_path = os.path.join(target_data_dir, clss)
        # e.g. data/ML_export/preprocessed_images/tele
        class_num = classes.index(clss)
        for idx, img in enumerate(tqdm(os.listdir(path))):
            try:
                id = os.path.splitext(img)
                id = id[0]
                new_file_name = f"{idx}_{id}" # .e.g 1_777777
                idx_ext = idx+7408
                new_file_name_r = f"{idx_ext}_{id}_r"
                target_file_name = f"{target_path}/{new_file_name}"
                # e.g. data/ML_export/preprocessed_images/tele/1_777777
                target_file_name_r = f"{target_path}/{new_file_name_r}"
                im = Image.open(os.path.join(path,img))
                w, h = im.size
                #print(id)
                #print("original size is:", w, h)

                #Rotate if in portrait mode
                if h > w:
                    angle = 90
                    im = im.rotate(angle, expand=True)
                    #w, h = im.size
                    #print("rotated image size is:", im.size)

                #resize and pad
                original_size = im.size  # old_size[0] is in (width, height) format
                ratio = float(max(target_size))/max(original_size)
                new_size = tuple([int(x*ratio) for x in original_size])
                im = im.resize(new_size, Image.ANTIALIAS)
                # create a new image and paste the resized on it
                new_im = Image.new('RGB', (target_size[0], target_size[1]))
                new_im.paste(im, ((target_size[0] - new_size[0])//2,(target_size[1] - new_size[1])//2))
                w, h = new_im.size
                new_im_r = new_im.transpose(PIL.Image.FLIP_LEFT_RIGHT)
                #new_im.show()
                #img_array = np.array(new_im) #.reshape(im.size[0], im.size[1], 1)
                #images.append([id, class_num, w, h, new_im])
                #training_data.append([new_im, class_num])
                new_im_r.save(target_file_name_r, format='JPEG')
                new_im.save(target_file_name, format='JPEG')
            except OSError as e:
                print("OSError. Bad image most likely", e, os.path.join(path,img))
                


# In[8]:


def adjust_learning_rate(epoch):

    lr = 0.001
    if epoch > 180:
        lr = lr / 1000000
    elif epoch > 150:
        lr = lr / 100000
    elif epoch > 120:
        lr = lr / 10000
    elif epoch > 90:
        lr = lr / 1000
    elif epoch > 30:
        lr = lr / 100
    elif epoch > 10:
        lr = lr / 10

    for param_group in optimizer.param_groups:
        param_group["lr"] = lr


# In[9]:


def save_models(epoch):
    torch.save(model.state_dict(), "LensClass_v0,1_model_{}.model".format(epoch))
    print("Checkpoint saved.")


# In[10]:


def test():
    with experiment.test():
        #correct = 0
        total = 0
        model.eval()
        test_acc = 0.0 # aka 'correct'
        for i, (images, labels) in enumerate(test_loader):

            if cuda_avail:
                    images = Variable(images.cuda())
                    labels = Variable(labels.cuda())

            #Predict classes using images from the test set
            outputs = model(images)
            _,prediction = torch.max(outputs.data, 1)
            prediction = prediction.cpu().numpy()
            total += labels.size(0)
            test_acc += torch.sum(torch.from_numpy(prediction).cuda() == labels.data)
            #test_acc += torch.sum(prediction == labels.data)

        #experiment.log_metric("accuracy", test_acc / total)

        #Compute the average acc and loss over all test images
        test_acc = test_acc / 2963
        experiment.log_metric("out of sample accuracy", test_acc)

        return test_acc


# In[11]:


def train(num_epochs):
    
    with experiment.train():
        step = 0
    
        #Print all hyperparameters
        print(hyper_params)

        best_acc = 0.0

        for epoch in range(num_epochs):
            correct = 0
            total = 0
            model.train()
            train_acc = 0.0 
            train_loss = 0.0
            for i, (images, labels) in enumerate(train_loader):
                #Move images and labels to gpu if available
                if cuda_avail:
                    images = Variable(images.cuda())
                    labels = Variable(labels.cuda())

                #Clear all accumulated gradients
                optimizer.zero_grad()
                #Predict classes using images from the test set
                outputs = model(images)
                #Compute the loss based on the predictions and actual labels
                loss = loss_fn(outputs,labels)
                #Backpropagate the loss
                loss.backward()

                #Adjust parameters according to the computed gradients
                optimizer.step()

                # compute train accuracy              
                train_loss += loss.cpu().item() * images.size(0)             
                _, prediction = torch.max(outputs.data, 1)
                
                batch_total = labels.size(0)
                total += batch_total

                train_acc += torch.sum(prediction == labels.data)
                #batch_correct = (predicted == labels.data).sum()
                correct += train_acc
                
                # Log batch_accuracy to Comet.ml; step is each batch
                step += 1
                #experiment.log_metric("batch_accuracy", train_acc / batch_total, step=step)

            # Log epoch accuracy to Comet.ml; step is each epoch
           #experiment.log_metric("epoch_accuracy", correct / total, step=epoch)
            
            #Call the learning rate adjustment function
            adjust_learning_rate(epoch)

            #Compute the average acc and loss over all training images
            train_acc = train_acc / 23706
            train_loss = train_loss / 23706
            
            experiment.log_metric("epoch_loss", train_loss)
            experiment.log_metric("epoch_acc", train_acc)
            #Evaluate on the test set
            test_acc = test()

            # Save the model if the test acc is greater than our current best
            if test_acc > best_acc:
                print("Test accuracy in this epoch is greater than the current best...")
                save_models(epoch)
                print("...model has been saved.")
                best_acc = test_acc


            # Print the metrics
            print("Epoch {}, Train Accuracy: {} , TrainLoss: {} , Test Accuracy: {}".format(epoch, train_acc, train_loss,test_acc))


# In[12]:


#helper function to show images from the dataset loader
def image_show(img):
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy() # convert to numpy array
    plt.imshow(np.transpose(npimg, (1, 2, 0))) # transpose numpy array
    #plt.figure(figsize=(1024, 680))
    img_name = f"logs/training_samples/smpl_{p_name}.jpg"
    plt.savefig(img_name, bbox_inches='tight')
    plt.close()
    #plt.show()


# In[13]:


#helper function to caluclate conv output size
def outputSize(in_size, kernel_size, stride, padding):
    output = int((in_size - kernel_size + 2*(padding)) / stride) + 1
    return(output)


# # Operations

# ## Load data

# In[14]:


#preprocess_training_data()


# In[15]:


# Transformation that will be handled by the torch.datasets sub class
# Order matters: first all PIL transformations, then transform to tensor, then normlaise
data_transform = transforms.Compose([
        transforms.Grayscale(num_output_channels=1),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5],
                             std=[0.5])        
    ])


# In[16]:


# Instantiate a class of the generic subclass .datasets.ImageFolder
master_dataset = datasets.ImageFolder(root='data/ML_export/preprocessed_images/',
                                           transform=data_transform)


# In[17]:


#create train, test, and validation split
n = len(master_dataset)  #  total elements you have
n_test = int( n * .1 ) # number of test items
n_val = int( n * .1 ) # number of val items
n_train = n - 2 * n_test
# use torch.datarandom_split
train_set, val_set, test_set = data.random_split(master_dataset, (n_train, n_val, n_test))
print('Training set is', n_train, 'items long.')
print('Test set is', n_test, 'items long.')
print('Validation set is', n_val, 'items long.')


# In[18]:


#create a test loader 
test_loader = torch.utils.data.DataLoader(test_set,
                                             batch_size = batch_size, shuffle=False,
                                             num_workers=4)


# In[19]:


#create a validation loader
val_loader = torch.utils.data.DataLoader(val_set,
                                             batch_size = batch_size, shuffle=False,
                                             num_workers=4)


# In[20]:


#create a train loader
train_loader = torch.utils.data.DataLoader(train_set,
                                             batch_size = batch_size, shuffle=True,
                                             num_workers=4)


# In[21]:


# get a new batch of images from the test loader
dataiter = iter(test_loader)
images, labels = dataiter.next()
images[0].size()

# saves a batch of images and their corresponding labels at /logs/samples
image_show(torchvision.utils.make_grid(images, normalize = 'True', range= (0, 255)))


corresponding_labels = ' '.join('%5s' % classes[labels[j]] for j in range(batch_size))
with open("logs/training_samples/label_maps.txt", "w") as text_file:
    print(f"Corresponding labels: {corresponding_labels}", file=text_file)


# ## Model setup

# In[22]:


# Check if GPU support is available
cuda_avail = torch.cuda.is_available()

# Create model
model = SimpleNet(num_classes=2)

#if CUDA is available, move the model to the GPU
if cuda_avail:
    model.cuda()
    
#Define the optimizer and loss function
optimizer = Adam(model.parameters(), lr=0.001, weight_decay=0.0001)
loss_fn = nn.CrossEntropyLoss()


# ## Train network

# In[23]:


train(epochs)


# In[ ]:




