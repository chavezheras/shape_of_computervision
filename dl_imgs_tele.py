#!/usr/bin/env python
# coding: utf-8

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#Lens-image-Classifier-overview" data-toc-modified-id="Lens-image-Classifier-overview-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Lens image Classifier overview</a></span></li><li><span><a href="#Imports" data-toc-modified-id="Imports-2"><span class="toc-item-num">2&nbsp;&nbsp;</span>Imports</a></span></li><li><span><a href="#Load-data" data-toc-modified-id="Load-data-3"><span class="toc-item-num">3&nbsp;&nbsp;</span>Load data</a></span></li><li><span><a href="#Variables" data-toc-modified-id="Variables-4"><span class="toc-item-num">4&nbsp;&nbsp;</span>Variables</a></span></li><li><span><a href="#Functions" data-toc-modified-id="Functions-5"><span class="toc-item-num">5&nbsp;&nbsp;</span>Functions</a></span></li><li><span><a href="#Operations" data-toc-modified-id="Operations-6"><span class="toc-item-num">6&nbsp;&nbsp;</span>Operations</a></span></li></ul></div>

# # Lens image Classifier overview
# 
# This notebook creates two lists of [Flickr](https://www.flickr.com/ "Flickr Homepage") IDs based on their lens/focal length, and downloads the corresponding images using the URLs from the [Visual Genome project](https://visualgenome.org/api/v0/api_home.html "Download Data").
# It requires the following:
# * The uncompressed `image_data.json` file to be in the `/data` directory of the project. This file can be downloaded from [here](https://visualgenome.org/api/v0/api_home.html "Download Data").
# * The dataset `photo_dataset` found in the `/data` directory created using the `analyse.ipynb` notebook.
# 

# # Imports

# In[45]:


import pandas as pd
from tqdm import tqdm
import random
import wget
import time
import warnings
import json


# # Load data

# In[2]:


# Load dataset
df = pd.read_hdf('data/photo_dataset.h5', 'df1')
#df = pd.read_csv('data/photo_dataset.csv')


# In[ ]:





# # Variables

# In[3]:


data_dump_file = 'data/image_data.json' # found in project folder path /data
urls = []
err_tele = []
err_wide = []


# # Functions

# In[4]:


# Load VG data dump
# opens image_data.json file in current working directory
# outputs a list object
def open_data_dump(data_dump_file):
    with open(data_dump_file) as json_file:
        global raw_data
        raw_data = json.load(json_file)
    print("Data dump loaded as <raw_data>")
    print("Length of data dump is", len(raw_data), "items.")
    print("This is an example of a random item in <raw_data>:")
    random_item = random.choice(raw_data)
    print(random_item)


# In[5]:


# takes the list object and strips it to contain only Flickr IDs and URLs
# outputs another list object 
def strip_data(raw_data):
    for item in raw_data:
        if item['flickr_id'] != None:
            ID = item['flickr_id']
            URL = item['url']
            info = [ID, URL]
            urls.append(info)
        else: pass
    print("Raw data has been stripped to ID and URL and is now list 'urls'.")


# In[41]:


# Downloader function, takes a list of URLs and a category e.g. 'wide'
# Downloads by default to data/ML_export/
def download_images(url_list, cat):
    if cat == 'tele':
        with tqdm(total=len(url_list)) as pbar:
            for item in url_list:
                try:
                    img = wget.download(item, out='data/ML_export/tele/')
                    print(img)
                    time.sleep(0.25)
                    pbar.update(1)
                except:
                    err_tele.append(item)
                    warnings.warn("An item could not be downloaded.")
    elif cat == 'wide':
        with tqdm(total=len(url_list)) as pbar:
            for item in url_list:
                try:
                    img = wget.download(item, out='data/ML_export/wide/')
                    print(img)
                    time.sleep(0.25)
                    pbar.update(1)
                except:
                    err_wide.append(item)
                    warnings.warn("An item could not be downloaded.")
    else:
        print("There is no category named", cat, "check the name and try again.")


# # Operations

# In[7]:


# wide angle slice
wide_df = df[df['FocalLength'] < 24]
wide_IDs = wide_df.index.tolist()
print(len(wide_IDs))


# In[8]:


# telephoto slice
tele_df = df[df['FocalLength'] > 135]
tele_IDs = tele_df.index.tolist()
tele_IDs = tele_IDs[:7408]
print(len(tele_IDs))


# In[9]:


open_data_dump(data_dump_file)


# In[10]:


strip_data(raw_data)


# In[11]:


URLdf = pd.DataFrame(urls, columns =['flickr_ID', 'vg_URL'])


# In[12]:


# slice tele to gather URLs
tele = URLdf[URLdf['flickr_ID'].isin(tele_IDs)]
tele.drop_duplicates(['flickr_ID'], keep='last', inplace=True)
#tele.info()
tele_URLs = tele['vg_URL'].tolist()
print("A list of telephoto URLs has been created as <tele_URLs>.")


# In[13]:


# slice wide to gather URLs
wide = URLdf[URLdf['flickr_ID'].isin(wide_IDs)]
wide.drop_duplicates(['flickr_ID'], keep='last', inplace=True)
#wide.info()
wide_URLs = wide['vg_URL'].tolist()
print("A list of wide URLs has been created as <wide_URLs>.")


# In[15]:


#test lists
'''
test_list_w = wide_URLs[:8]
test_list_w.append('https://cs.stanford.edu/people/rak248/VG_100K_2/does_not_exist.jpg')
test_list_w.append('https://cs.stanford.edu/people/rak248/VG_100K/2353882.jpg')

test_list_t = tele_URLs[:8]
test_list_t.append('https://cs.stanford.edu/people/rak248/VG_100K_2/does_not_exist.jpg')
test_list_t.append('https://cs.stanford.edu/people/rak248/VG_100K/2353882.jpg')
'''


# In[42]:


download_images(tele_URLs, cat='tele')


# In[39]:


with open('logs/log_tele.txt', 'w') as f:
    for item in err_tele:
        f.write("%s\n" % item)


# In[44]:


'''with open('logs/log_wide.txt', 'w') as f:
    for item in err_wide:
        f.write("%s\n" % item)
        '''

