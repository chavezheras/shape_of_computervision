#!/usr/bin/env python
# coding: utf-8

# ## Main EXIF extractor  
# 
# This script takes [Flickr](https://www.flickr.com/ "Flickr Homepage") IDs and outputs their [EXIF](https://en.wikipedia.org/wiki/Exif/ "What is EXIF?") info.  
# 
# It requires the the uncompressed `image_data.json` file to be in the `/data` directory of the project. This file can be downloaded from [here](https://visualgenome.org/api/v0/api_home.html "Download Data").

# ### Imports ###

# In[ ]:


import json
import pandas as pd
import flickrapi
import pprint
from tqdm import tqdm
import time
import sys
import pickle
import os
import urllib.request
import pprint
import random


# ### Variables ###

# In[ ]:


pp = pprint.PrettyPrinter(indent=2)
data_dump_file = 'data/image_data.json' # found in project folder path /data
images =[]
#Flickr credentials
api_key = '' #Your API key here
api_secret = ''  #Your API secret key here
flickr = flickrapi.FlickrAPI(api_key, api_secret, format='parsed-json')
data = {}
not_available = 0


# ### structure of FLickr API response ###
# 
# ~~~~
# #Camera brand
# my_photo
# 
# #Camera model
# my_photo['photo']['camera']
# 
# #Exposure time
# my_photo['photo']['exif'][9]['raw']['_content']# 9th element is exposure dict
# 
# #Aperture
# my_photo['photo']['exif'][10]#['raw']['_content']# 10th element is aperture dict
# 
# #Timestamp
# my_photo['photo']['exif'][16]['raw']['_content'] #original timestamp
# 
# #Timestamp
# my_photo['photo']['exif'][17]['raw']['_content'] #digitised timestamp
# 
# my_photo['photo']['exif'][22]['raw']['_content'] # focal lenght
# 
# my_photo['photo']['exif'][36]['raw']['_content'] # lens model
# ~~~~

# ### Functions ###

# In[ ]:


# opens image_data.json file in current working directory
# outputs a list object
def open_data_dump(data_dump_file):
    with open(data_dump_file) as json_file:
        global raw_data
        raw_data = json.load(json_file)
    print("Data dump loaded as <raw_data>")
    print("Length of data dump is", len(raw_data), "items.")
    print("This is an example of a random item in <raw_data>:")
    random_item = random.choice(raw_data)
    pp.pprint(random_item)


# In[ ]:


# takes the list object and strips it to contain only Flickr IDs and URLs
# outputs another list object 
def strip_data(raw_data):
    for item in raw_data:
        if item['flickr_id'] != None:
            ID = item['flickr_id']
            URL = item['url']
            info = [ID, URL]
            images.append(info)
        else: pass


# In[ ]:


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


# In[ ]:


def batch_load(images):
    global part
    global part_str
    with open('images.txt', 'w') as filehandle:
        json.dump(images, filehandle)
        print("<raw_data> has been stripped to <images> and written to         <images.txt.> file in project path.")
        part11 = images[100000:103078]
        images = images[:100000]
        part1, part2, part3, part4, part5, part6, part7, part8,         part9, part10 = (part for part in chunks(images, 10000))
        print("<images> has been split into 11 separate list objects, each with         Flickr ID at index 0 and VG URL at index 1.")
        print("Length of <part 1> is", len(part1), "items.")
        print("Length of <part 7> is", len(part7), "items.")
        print("Length of <part 11> is", len(part11), "items.")
        print("The sum of all parts is", len(part1+part2+part3+part4+part5+                                             part6+part7+part8+part9+part10+part11),               "items.")
        print("This is an example of a random item in <part7>:")
        random_item_img = random.choice(part7)
        pp.pprint(random_item_img)
        ui = input("Which part do you want to scrape next? (1-11)")
        ui = int(ui)
        if ui == 1:
            part = part1
        elif ui == 2:
            part = part2
        elif ui == 3:
            part = part3
        elif ui == 4:
            part = part4
        elif ui == 5:
            part = part5
        elif ui == 6:
            part = part6
        elif ui == 7:
            part = part7
        elif ui == 8:
            part = part8
        elif ui == 9:
            part = part9
        elif ui == 10:
            part = part10
        elif ui == 11:
            part = part11
        else:
            print("Please enter a number 1-11")
        print("You have slected part", ui)
        part_str = "part{0}".format(ui)
        print("Type of <part_str>", type(part_str))
        print("Type of <part>", type(part))
        print("The legnth of part", ui, "is", len(part))


# In[ ]:


# takes a number of seconds to halt the scraping from Flickr (as per API limits)
def take_a_break(how_long):
    print("The programme will now take a break...")
    for remaining in range(how_long, 0, -1):
        sys.stdout.write("\r")
        sys.stdout.write("{:2d} seconds before resuming call to         Flcikr API...".format(remaining))
        sys.stdout.flush()
        time.sleep(1)

    sys.stdout.write("\rResuming download...                                     \n")


# In[ ]:


#data = {}
#not_available = 0
#iterIDs = iter(IDs)
#number_of_pictures = len(IDs)

# main EXIF extractor function, takes two arguments: 
# "limit" e.g. "3500" per unique Flickr API KEY
# "d_time" e.g. "3600" number of seconds before resuming download
# outputs a dictionary object <data> with ID as KEY and EXIF info
def get_exif(which_part, limit = 3500, d_time = 3600):
    IDs = which_part
    iterIDs = iter(IDs)
    number_of_pictures = len(IDs)
    try:
        time.sleep(2)
        print("Chose a call rate for the Flickr API (defaults to official API limits)...")
        time.sleep(1)
        print("To use default values just press 'enter'...")
        time.sleep(1)
        print("See: https://www.flickr.com/services/developer/api/ for more information...")
        time.sleep(2)
        limit = int(input("Input a batch size of API calls (default is 3500):"))
        d_time = int(input("Input the time in seconds to leave between each batch of calls (default is one hour):"))
        if limit == "":
            print("You have selected the default batch size of 3500 API calls.")
        if d_dime == "":
            print("You have selected the default time between batches of calls of one hour.")
        else:
            time.sleep(2)
            print("You have selected to call the Flickr API", limit, "times, every", d_time, "seconds.")
    except:
        pass
        
    counter = 0
    list_of_photos = []
    global not_available
    with tqdm(total=number_of_pictures) as pbar:
        for item in iterIDs:
            counter += 1
            try:
                ID = item[0]
                photo = flickr.photos.getExif(photo_id=ID)
                exif = photo['photo']['exif']
                list_of_photos.append(exif)
                for element in list_of_photos:
                    elements = {}
                    for entry in element:
                        if entry['tag'] == 'Make':
                            mk = entry['raw']['_content']
                            Make_dict = {'value':mk}
                            elements['Make'] = Make_dict

                        if entry['tag'] == 'Model':
                            mod = entry['raw']['_content']
                            MOD_dict = {'value':mod}
                            elements['CameraModel'] = MOD_dict

                        if entry['tag'] == 'FocalLength':
                            fl = entry['raw']['_content']
                            FL_dict = {'value':fl}
                            elements['FocalLength'] = FL_dict

                        if entry['tag'] == 'FNumber':
                            ap = entry['raw']['_content']
                            AP_dict = {'value':ap}
                            elements['Aperture'] = AP_dict

                        if entry['tag'] == 'ExposureTime':
                            ex = entry['raw']['_content']
                            EX_dict = {'value':ex}
                            elements['Exposure'] = EX_dict

                        if entry['tag'] == 'DateTimeOriginal':
                            ts = entry['raw']['_content']
                            TS_dict = {'value':ts}
                            elements['TimeStamp'] = TS_dict
                        
                        if entry['tag'] == 'LensModel':
                            lens = entry['raw']['_content']
                            LENS_dict = {'value':lens}
                            elements['Lens'] = LENS_dict

                data[ID] = elements
            except: 
                not_available += 1
            pbar.update(1)
            if counter % limit == 0:
                print("A batch of" ,limit, "items has been processed...")
                take_a_break(d_time)       


# In[ ]:


# takes the object data from the main extracting function and returns some basic stats
def get_stats(which_part,data):
    IDs = which_part
    to_scrape = (len(IDs))
    scraped = (len(data))
    no_exif = (len(IDs)) - ((len(data)) + not_available)
    total_imgs =  not_available + scraped + no_exif
    percentage = (scraped/to_scrape)*100
    random_photo_ID = random.choice(list(data))
    
    print('Total number of IDs processed:', total_imgs)
    #print(" ")
    #print('Total number of IDs processed:', to_scrape) #test
    print(" ")
    print('Acessible images:', scraped)
    print(" ")
    print('Image IDs to which access was denied:', not_available)
    print(" ")
    print("Accessible images where none of the requested EXIF     fields were available:", no_exif)
    print(" ")
    print('Accessible images with at least one of the requested     EXIF field available:', scraped)
    print(" ")
    print(percentage,'% of processed IDs have at least one of the rquested EXIF fields.')
    print(" ")
    print("·······")
    print(" ")
    
    try:        
        info_random_photo = data[random_photo_ID]
        print("RANDOM IMAGE RESPONSE SAMPLE")
        pp.pprint(info_random_photo)
    except:
        print("No EXIF info, try again.")


# In[ ]:


# takes the <data> object from the extracting function
# takes input from user to name the serialised file
# returns a serialised file at the current working directory
def serialise(which_name, data):
    pickle_file_name = which_name
    pickle_file_name = "data/" + pickle_file_name + ".pickle"
    #print(file_name)
    with open(pickle_file_name, 'wb') as handle:        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)
    print("<data> object has been serialised to the file:", pickle_file_name)
    #print("You can now use the <analyse.ipynb> Jupyter notebook to perform \
    #initial exloratory analysis.")


# ### Operations ###

# In[ ]:


open_data_dump(data_dump_file) # open the data dump from json file


# In[ ]:


strip_data(raw_data) # strip data to ID and URL


# In[ ]:


batch_load(images)


# In[ ]:


get_exif(part) # main extraction of EXIF from Flickr API


# In[ ]:


serialise(part_str, data) # serialises data into a pickle file in current working directory


# In[ ]:


get_stats(part, data) # print basic stats from <data>

