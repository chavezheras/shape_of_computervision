#!/usr/bin/env python
# coding: utf-8

# ## Main EXIF extractor  
# 
# This script takes [Flickr](https://www.flickr.com/ "Flickr Homepage") IDs and outputs their [EXIF](https://en.wikipedia.org/wiki/Exif/ "What is EXIF?") info.  
# 
# It requires the the uncompressed `image_data.json` file to be in the `/data` directory of the project. This file can be downloaded from [here](https://visualgenome.org/api/v0/api_home.html "Download Data").

# ### Imports ###

# In[1]:


import json
import pandas as pd
import flickrapi
import pprint
from tqdm import tqdm
import time
import sys
import pickle
import os
import urllib.request
import pprint
#import sqlite3
import random
#from itertools import islice


# ### Variables ###

# In[2]:


pp = pprint.PrettyPrinter(indent=2)
data_dump_file = 'data/image_data.json' # found in project folder path /data
images =[]

#Flickr credentials
api_key = '' #Your Flcikr API here
api_secret = '' #Your Flcikr API secret here
flickr = flickrapi.FlickrAPI(api_key, api_secret, format='parsed-json')
data = {}
not_available = 0


# ### structure of FLickr API response ###
# 
# ~~~~
# #Camera brand
# my_photo
# 
# #Camera model
# my_photo['photo']['camera']
# 
# #Exposure time
# my_photo['photo']['exif'][9]['raw']['_content']# 9th element is exposure dict
# 
# #Aperture
# my_photo['photo']['exif'][10]#['raw']['_content']# 10th element is aperture dict
# 
# #Timestamp
# my_photo['photo']['exif'][16]['raw']['_content'] #original timestamp
# 
# #Timestamp
# my_photo['photo']['exif'][17]['raw']['_content'] #digitised timestamp
# 
# my_photo['photo']['exif'][22]['raw']['_content'] # focal lenght
# 
# my_photo['photo']['exif'][36]['raw']['_content'] # lens model
# ~~~~

# ### Functions ###

# In[3]:


# opens image_data.json file in current working directory
# outputs a list object
def open_data_dump(data_dump_file):
    with open(data_dump_file) as json_file:
        global raw_data
        raw_data = json.load(json_file)
    print("Data dump loaded as <raw_data>")
    print("Length of data dump is", len(raw_data), "items.")
    print("This is an example of a random item in <raw_data>:")
    random_item = random.choice(raw_data)
    pp.pprint(random_item)


# In[4]:


# takes the list object and strips it to contain only Flickr IDs and URLs
# outputs another list object 
def strip_data(raw_data):
    for item in raw_data:
        if item['flickr_id'] != None:
            ID = item['flickr_id']
            URL = item['url']
            info = [ID, URL]
            images.append(info)
        else: pass
    with open('images.txt', 'w') as filehandle:
        json.dump(images, filehandle)
        print("<raw_data> has been stripped to <images> and written to <images.txt.> file in project path.")
        print("<images> is a list of lists with Flickr ID at index 0 and VG URL at index 1.")
        print("Length of <images> is", len(images), "items.")
        print("This is an example of a random item in <images>:")
        random_item_img = random.choice(images)
        pp.pprint(random_item_img)


# In[5]:


# takes user input to decide how many images to process
# outputs the selected number of Flickr IDs as the <IDs> object, and appends a known image for control
def test_images():
    all_the_images = len(images)
    global IDs
    while True:
        try:
            test_use = input("Do you want to scrape all available images? [y/n]")
            
            if test_use == 'no':
                number_of_test_IDs = input("How many images do you want to scrape?")
                try:
                    number_of_test_IDs = int(number_of_test_IDs)
                except ValueError:
                    print("Please input an integer number.") 
                    continue
                if number_of_test_IDs > 0 < all_the_images:   
                    number_of_test_IDs = number_of_test_IDs - 1
                    test_IDs = images[0:number_of_test_IDs]
                    my_photo = [24151449220,'https://www.flickr.com/photos/mrsoames/24151449220/in/dateposted-public/']
                    test_IDs.append(my_photo)
                    IDs = test_IDs
                    number_of_pictures = len(IDs)
                    print("You selected to use a set of", len(IDs), 'IDs, out of a possible', all_the_images)
                    print('This is', round(((len(IDs)*100)/all_the_images), 2), '% of all images in the Visual Genome dataset.')
                    break
                else:
                    print("Select a valid number from 1 to", all_the_images)
                    continue
                    
            if test_use == 'yes':
                my_photo = [24151449220,'https://www.flickr.com/photos/mrsoames/24151449220/in/dateposted-public/']
                IDs = images
                IDs.append(my_photo)
                number_of_pictures = len(IDs)
                print("You selected to extract info for all", all_the_images, "Flickr IDs from the Visual Genome dataset.")
                break
            
            if test_use == 'q':
                print("You have not selected any images.")
                break
            else:
                raise TypeError
        except TypeError:
            print("You need to type <yes> or <no>. Please try again, or type <q> to quit.")
            continue
        except EOFError:
            print("Please input something....")
            continue
        


# In[6]:


# takes a number of seconds to halt the scraping from Flickr (as per API limits)
def take_a_break(how_long):
    print("The programme will now take a break...")
    for remaining in range(how_long, 0, -1):
        sys.stdout.write("\r")
        sys.stdout.write("{:2d} seconds before resuming call to Flcikr API...".format(remaining))
        sys.stdout.flush()
        time.sleep(1)

    sys.stdout.write("\rResuming download...                                     \n")


# In[7]:


#data = {}
#not_available = 0
#iterIDs = iter(IDs)
#number_of_pictures = len(IDs)

# main EXIF extractor function, takes two arguments: 
# "limit" e.g. "3500" per unique Flickr API KEY
# "d_time" e.g. "3600" number of seconds before resuming download
# outputs a dictionary object <data> with ID as KEY and EXIF info
def get_exif(limit = 3500, d_time = 3600):
    iterIDs = iter(IDs)
    number_of_pictures = len(IDs)
    try:
        time.sleep(2)
        print("Chose a call rate for the Flickr API (defaults to official API limits)...")
        time.sleep(1)
        print("To use default values just press 'enter'...")
        time.sleep(1)
        print("See: https://www.flickr.com/services/developer/api/ for more information...")
        time.sleep(2)
        limit = int(input("Input a batch size of API calls (default is 3500):"))
        d_time = int(input("Input the time in seconds to leave between each batch of calls (default is one hour):"))
        if limit == "":
            print("You have selected the default batch size of 3500 API calls.")
        if d_dime == "":
            print("You have selected the default time between batches of calls of one hour.")
        else:
            time.sleep(2)
            print("You have selected to call the Flickr API", limit, "times, every", d_time, "seconds.")
    except:
        pass
        
    counter = 0
    list_of_photos = []
    global not_available
    with tqdm(total=number_of_pictures) as pbar:
        for item in iterIDs:
            counter += 1
            try:
                ID = item[0]
                photo = flickr.photos.getExif(photo_id=ID)
                exif = photo['photo']['exif']
                list_of_photos.append(exif)
                for element in list_of_photos:
                    elements = {}
                    for entry in element:
                        if entry['tag'] == 'Make':
                            mk = entry['raw']['_content']
                            Make_dict = {'value':mk}
                            elements['Make'] = Make_dict

                        if entry['tag'] == 'Model':
                            mod = entry['raw']['_content']
                            MOD_dict = {'value':mod}
                            elements['CameraModel'] = MOD_dict

                        if entry['tag'] == 'FocalLength':
                            fl = entry['raw']['_content']
                            FL_dict = {'value':fl}
                            elements['FocalLength'] = FL_dict

                        if entry['tag'] == 'FNumber':
                            ap = entry['raw']['_content']
                            AP_dict = {'value':ap}
                            elements['Aperture'] = AP_dict

                        if entry['tag'] == 'ExposureTime':
                            ex = entry['raw']['_content']
                            EX_dict = {'value':ex}
                            elements['Exposure'] = EX_dict

                        if entry['tag'] == 'DateTimeOriginal':
                            ts = entry['raw']['_content']
                            TS_dict = {'value':ts}
                            elements['TimeStamp'] = TS_dict
                        
                        if entry['tag'] == 'LensModel':
                            lens = entry['raw']['_content']
                            LENS_dict = {'value':lens}
                            elements['Lens'] = LENS_dict

                data[ID] = elements
            except: 
                not_available += 1
            pbar.update(1)
            if counter % limit == 0:
                print("A batch of" ,limit, "items has been processed...")
                take_a_break(d_time)       


# In[8]:


# takes the object data from the main extracting function and returns some basic stats
def get_stats(data):
    to_scrape = (len(IDs))
    scraped = (len(data))
    no_exif = (len(IDs)) - ((len(data)) + not_available)
    total_imgs =  not_available + scraped + no_exif
    percentage = (scraped/to_scrape)*100
    random_photo_ID = random.choice(list(data))
    
    my_photo_FL = data[24151449220]['FocalLength']['value']
    my_photo_Make = data[24151449220]['Make']['value']
    my_photo_CModel = data[24151449220]['CameraModel']['value']
    my_photo_Lens = data[24151449220]['Lens']['value']
    
    print('Total number of IDs processed:', total_imgs)
    #print(" ")
    #print('Total number of IDs processed:', to_scrape) #test
    print(" ")
    print('Acessible images:', scraped)
    print(" ")
    print('Image IDs to which access was denied:', not_available)
    print(" ")
    print("Accessible images where none of the requested EXIF fields were available:", no_exif)
    print(" ")
    print('Accessible images with at least one of the requested EXIF field available:', scraped)
    print(" ")
    print(percentage,'% of processed IDs have at least one of the rquested EXIF fields.')
    print(" ")
    print("·······")
    print(" ")
    print("CONTROL IMAGE INFO")
    print("The camera manufacturer of my photo is:", my_photo_Make)
    print("The camera model of my photo is:", my_photo_CModel)
    print("The lens model of my photo is:", my_photo_Lens)
    print("The focal length of my photo is:", my_photo_FL) #test
    print(" ")
    print("Control Flickr image available at: https://www.flickr.com/photos/mrsoames/24151449220/in/dateposted-public/")
    print("·······")
    print(" ")
    print(" ")
    
    try:        
        info_random_photo = data[random_photo_ID]
        print("RANDOM IMAGE RESPONSE SAMPLE")
        pp.pprint(info_random_photo)
    except:
        print("No EXIF info, try again.")


# In[9]:


# takes the <data> object from the extracting function
# takes input from user to name the serialised file
# returns a serialised file at the current working directory
def serialise(data):
    pickle_file_name = str(input("Chose a file name:"))
    pickle_file_name = "data/" + pickle_file_name + ".pickle"
    #print(file_name)
    with open(pickle_file_name, 'wb') as handle:        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)
    print("<data> object has been serialised to the file:", pickle_file_name)
    print("You can now use the <analyse.ipynb> Jupyter notebook to perform initial exloratory analysis.")


# In[10]:


# takes the pickle file from the serialise function and reads it back as a <data> object
def deserialise():
    in_pickle_file = str(input("Name of .pickle file to load:"))
    in_pickle_file = "data/" + in_pickle_file + ".pickle"
    try:
        with open(in_pickle_file, 'rb') as handle:            data = pickle.load(handle)
        print("The file", in_pickle_file, "has been loaded as <data>.")
        get_stats(data)
    except:
        print("Make sure the pickle file name is correct.")


# ### Operations ###

# In[11]:


open_data_dump(data_dump_file) # open the data dump from json file


# In[12]:


strip_data(raw_data) # strip data to ID and URL


# In[13]:


test_images() # takes number of images to be processed as input from user


# In[14]:


get_exif() # main extraction of EXIF from Flickr API


# In[15]:


serialise(data) # serialises data into a pickle file in current working directory


# In[16]:


get_stats(data) # print basic stats from <data>

