# The implicit optical perspective of computer vision

## Overview
This is the companion prototype to the article ["On machine vision and photographic imagination"](https://doi.org/10.1007/s00146-020-01091-y) by [Daniel Chavez Heras](http://elusivepixel.com/) and [Tobias Blanke](https://www.kcl.ac.uk/people/professor-tobias-blanke). The project proposes a method to research what we call "implicit optical perspective" in general purpose computer vision systems.

If you use the dataset or code in this repository, please cite the avobe article as the source and credit the authors accordingly. 
And if you train your own model using this data, please let us know!

## Getting started
1. Clone repository into a local folder: `git clone https://gitlab.com/chavezheras/shape_of_computervision`
2. Install `pipenv` if you have not already. E.g. `pip install pipenv` (see [Pipenv](https://github.com/pypa/pipenv "Pipenv") for more information).
3. Run `pipenv install` to create a virtual environment and install all required dependencies.
4. Activate virtual enviornment by running `pipenv shell`

## Modules
To replicate the whole experiment you need to follow these steps in order. Each setp comes with a corresponding Jupyter notebook (see [Jupyter Notebook](https://jupyter.org/) for more information):

1. Scrape EXIF metadata from a set of Flickr images --> use `exif_extractor.ipynb` or `batch_extractor.ipynb`
2. Clean and analyse the scraped data --> use `analysis.ipynb`
3. Assemble the dataset --> use `image_downloader.ipynb`
4. Train a focal length classifier --> `lens_ classifier.ipynb` (Requires a CUDA enabled GPU)

Each notebook is annotated and can be used interactively. However, for some processes (e.g. training, or downloading), it is advisable to produce stand-alone `.py` scripts. Some of these are also included.

Alternatively, you can test the results only by running `predict.py` over any image. This will load one of the provided model checkpoints, perform inference, and output a prediction. Alternatively, you can access the EXIF dataset to perform your own analysis by downloading `photo_dataset.csv` or `photo_dataset.h5` (key='df1').  \

This is an example of a successful prediction:

![example](data/images/docs/fl_comparsion_3.jpg "example of successful inference")  

The image on the left was taken with a mobile phone (28mm); the one on the right with a DSLR (340mm)


## Data 
This project uses [Flickr](https://www.flickr.com/ "Flickr Homepage") IDs from the [Visual Genome](https://visualgenome.org/ "Visual Genome Homepage") dataset, and extracts their [EXIF](https://en.wikipedia.org/wiki/Exif/ "What is EXIF?") metadata through [Flickr's API](https://github.com/alexis-mignon/python-flickr-api/). This is how EXIF info looks on Flickr:

![example](data/images/docs/example.png "example of EXIF info in Flickr")    


This is an (abbreviated) example of Flickr's API response:

```
[
 { 'label': 'Make',
    'raw': {'_content': 'Canon'},
    'tag': 'Make',
    'tagspace': 'IFD0',
    'tagspaceid': 0},
  { 'label': 'Model',
    'raw': {'_content': 'Canon EOS REBEL T5i'},
    'tag': 'Model',
    'tagspace': 'IFD0',
    'tagspaceid': 0},
  { 'clean': {'_content': '50 mm'},
    'label': 'Focal Length',
    'raw': {'_content': '50.0 mm'},
    'tag': 'FocalLength',
    'tagspace': 'ExifIFD',
    'tagspaceid': 0},
  { 'label': 'Lens Model',
    'raw': {'_content': 'EF50mm f/1.8 II'},
    'tag': 'LensModel',
    'tagspace': 'ExifIFD',
    'tagspaceid': 0},
]
```



This is an example of the resulting dataframe:

| Camera manufacturer | Camera model | Exposure | Aperture | Focal Length  |
| ------------- |:-------------:| -----:| --- | ---| 
| Canon | Canon PowerShot S2 IS | 1/640 | f4 | 72mm  |
| Panasonic | DMC-FX9 | 1/640 | f3.6 | 10mm  |
| Canon | Canon EOS 20D | 1/1200 | f11 | 560mm  |
| Nikon | NIKON D50 | 1/250 | f5 | 125mm  |
| Canon | Canon PowerShot SD600 | 1/800 | f2.8 | 5.8mm  |
| ...| ... | ... | ... | ... |  



## Dependencies
Ideally, all dependencies will be managed through `pipenv`. But if that's not available to you, here is a list:

Required:
* Pyhton 3
* torch
* pytorch-lightning
* torchvision
* pillow (Version 7 or avobe can clash with Torch, if unsure use 6.x)
* pandas
* flickr-api
* jupyter
* tqdm
* wget
* comet-ml
* tables 
 
Optional:
* jupyterthemes
* jupyter-contrib-nbextensions
* jupyter-nbextensions-configurator
* pprint
* opencv-python


